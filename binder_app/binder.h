#ifndef __BINDER_H__
#define __BINDER_H__

#define DECLARE_BINDER_MAP() \
@property (nonatomic, retain)          NSMutableDictionary*    _binder_dict_; \
- (void) BIND: (NSString*)property1 BINDED:(id)property2; \


#define DECLARE_BINDER_LOOP() \
- (void) BIND: (NSString*)property1 BINDED:(id)property2 \
{ \
    if([property1 isEqualToString:property2]) \
        return; \
    \
    if(self._binder_dict_ == nil) \
    { \
        self._binder_dict_ = [[[NSMutableDictionary alloc] init] autorelease]; \
    } \
    [self._binder_dict_ setObject: property2 forKey: property1]; \
    [self addObserver:self \
           forKeyPath:property1 \
              options:0 \
              context:nil];\
} \
@synthesize _binder_dict_ = __binder_dict_; \
 \
- (void)observeValueForKeyPath:(NSString*)keyPath \
ofObject:(id)object \
change:(NSDictionary*)change \
context:(void*)context \
{ \
    NSArray *chunks = [[self._binder_dict_ objectForKey: keyPath] componentsSeparatedByString: @"."]; \
    id obj = self; \
    for(int i = 0; i < chunks.count - 1; i++) \
    { \
        obj = [obj valueForKey:[chunks objectAtIndex:i]]; \
    } \
    [obj setValue:[self valueForKey:keyPath] forKey:[chunks lastObject]]; \
} \

#endif // __BINDER_H__