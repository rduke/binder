//
//  ViewController.h
//  binder_app
//
//  Created by farid on 15.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import "binder.h"

@interface ViewController : UIViewController

@property (nonatomic, retain) IBOutlet UITextField*            textField;
@property (nonatomic, retain) IBOutlet UILabel*                label;
@property (nonatomic, retain)          User*                   user;
@property (nonatomic, retain)          NSString*               simpleText;

DECLARE_BINDER_MAP();

- (IBAction)textFieldChanged;

@end
