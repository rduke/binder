//
//  ViewController.m
//  binder_app
//
//  Created by farid on 15.08.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize label      = _label;
@synthesize textField  = _textField;
@synthesize user       = _user;
@synthesize simpleText = _simpleText;


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.user = [[User alloc] autorelease];
    [self BIND:@"simpleText" BINDED:@"label.text"];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (IBAction)textFieldChanged
{
    self.simpleText = self.textField.text;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

DECLARE_BINDER_LOOP()

@end
